import { options } from "./lib/argumentParser.js";
import { OfficeFile } from "./lib/office-file.js";
import { pdfConversion } from "./lib/pdfConversion.js";
import { replace } from "./lib/replace.js";
import { Spreadsheet } from "./lib/spreadsheet.js";

// data file
const spreadsheet = new Spreadsheet(options.spreadsheet);
if (options.verbosity > 0) console.log(`found ${spreadsheet.length} entries.`);

// template file
const file = new OfficeFile();
await file.loadContent(options.template);

// filtering
console.log("Applying filters");
const filteredData = spreadsheet.getFilteredData(options.query);
console.log(`After applying filters, ${filteredData.length} of ${spreadsheet.data.length} item(s) remain.`);

// processing
for (const index in filteredData) {
	const entry = Object.assign({}, {index}, filteredData[index]); // adds the index to a clone of the object.
	const filename = replace(options.output, entry, false).replace(/[^a-z0-9. -]/gi, "-");

	if (options.verbosity > 0) console.log(`replacing for ${filename} ...`);
	const newContent = replace(file.content, entry, true, options.verbosity > 1);

	if (!options.dryRun) {
		await file.storeContentAs(newContent, filename, options.force);
	}
	if (options.verbosity > 0) console.log(`... done writing ${filename}`);
	if (options.pdf) {
		if (options.verbosity > 0) console.log(`... creating pdf ${filename}.pdf`);
		if (!options.dryRun) {
			await pdfConversion(filename, options.force); // we don't strictly need to await this, but the logs are more readable this way.
		}
		if (options.verbosity > 0) console.log("... pdf complete");
	}
}
