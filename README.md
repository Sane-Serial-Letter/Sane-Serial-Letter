# Sane Serial Letter

LibreOffice's serial letter is a pain to set up. It is way to convoluted with data sources tied to fields that can only be filled by one data source. Changing that data source? Nice, you now get to redo all your templates.

Sane Serial Letter is a much simpler approach. It just replaces strings that look like `{{name}}` with whatever is in the column `name` from some spreadsheet. Done. Your templates are not tied to one specific data source. There are no requirements to how you organize your data sources.

![Screenshot of a template with all its rendered output files](demo-files/demo-output-screenshot.png)


## Requirements

* Node JS (tested with 16+) including npm
* some open document text file template (for example `demo-files/template.odt`)
* some open document spreadsheet (for example `demo-files/addresses.ods`) - this project will probably also read CSV or xls.
* (optional) installed libreoffice, in case you want to convert to PDF.


## Setup

* `npm install`


## Usage

### Basics

`node index.js --template (template file name) --spreadsheet (spreadsheet file name) --output (output file names)`

so out of the box, this should work:

`node index.js --template demo-files/template.odt --spreadsheet demo-files/addresses.ods --output "demo-output/{{zip}} {{name}}.odt"`


### Additional options

You can increase the verbosity to see what it does. Just add `--verbosity 2` to the command.

Add `--pdf` to the command, to have pdfs created along the way. (requires installed libreoffice!)

Add `--output "path/{{filename with template}} or literal strings.odt"` to customize the output filename.

Add `--dry-run` to skip actual writing of files. This lets you get a good overview of what's about to happen. Great to combine it with `--verbosity` at level 1 or 2.

Add `--force` if you want to overwrite existing files.

Add `--query "something>value"` to filter your input spreadsheet. See below for more details.


### Querying

In a long list, you might want to apply some kind of filter. Maybe you just want the "new" lines, or you need just the lines from one city. Add a query to do this. You can have as many queries as you like, but any line needs to match ALL queries to be included!

`node index.js --query "name>C" --template demo-files/template.odt --spreadsheet demo-files/addresses.ods --output "demo-output/{{zip}} {{name}}.odt"`

This will include only Ted and Hari from the demo-addresses. Alina, Beckett and Billy will be filtered out.

Another query can be added like this:

`node index.js --query "name>C" "planet=Earth" --template demo-files/template.odt --spreadsheet demo-files/addresses.ods --output "demo-output/{{zip}} {{name}}.odt"`

This will leave only Ted in the resulting set, as he's the only one matching both queries.

Available comparisons are:

* `fieldname>comparison` the value in `fieldname` must be greater than the comparison value
* `fieldname<comparison` the value in `fieldname` must be less than the comparison value
* `fieldname!comparison` the value in `fieldname` must be something other than the comparison value
* `fieldname=comparison` the value in `fieldname` must be exactly the same sa the comparison value
* `fieldname` Specifying *just* the field name will check for "not empty":
  `node index.js --query "shoesize" --template demo-files/template.odt --spreadsheet demo-files/addresses.ods --output "demo-output/{{zip}} {{name}}.odt"`
  In our demo set, this would leave only Billy Butcher Beckett Mariner and Alina Starkov, the other ones do not have shoesize set at all.


## LICENSE

This is covered by the [AGPL-v3 License](LICENSE-agpl-3.0.txt). This of course only refers to code in this repository and _not_ to documents you process through it.
