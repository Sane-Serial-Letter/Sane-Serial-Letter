import { program, InvalidArgumentError } from "commander";

const QUERY_PARTS = /^(?<field>[a-zA-Z0-9]+)((?<comparison>[<>=!])(?<value>[^\s]+))?$/i;

function queryParser(input, previous) {
	const match = input.match(QUERY_PARTS);
	if (match) {
		return previous.concat(match.groups);
	} else {
		throw new InvalidArgumentError(`can't parse your query ${input}`);
	}
}

program
	.name("Sane Serial Letter")
	.description("Creates a serial letter from nothing but a template file and a spreadsheet.")
	.requiredOption("-t --template <path>", "path to your template file")
	.requiredOption("-s --spreadsheet <path>", "path to your spreadsheet - should work with .ods or .xlsx")
	.option("-o --output <\"{{field1}}/{{field2}}.odt\">", "specify how to construct the output filenames. May include a path which must exist.")
	.option("-n --dry-run", "perform a trial run that does not write any files")
	.option("-f --force", "overwrite existing files")
	.option("-p --pdf", "also convert result to pdf")
	.option("-q --query [query...]", "filter list by a query", queryParser, [])
	.option("-v --verbosity [level]", "prints out more information", parseInt, 0)
	.addHelpText("after", `
Example Query:
  [... other options ...] --query "name>Q" "year>2021"
  should filter the column "name" for lines greater than Q and column "year" for lines greater than 2021`);

program.parse();

export const options = program.opts();

if (!options.output || options.output === options.template || options.output === options.spreadsheet) {
	const bits = options.template.split(".");
	bits.splice(bits.length-1, 0, "output - {{index}}");
	options.output = bits.join(".");
}
