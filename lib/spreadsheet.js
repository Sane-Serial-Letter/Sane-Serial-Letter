import XLSX from "xlsx";

export class Spreadsheet {
	constructor(filename) {
		this.filename = filename;
		this.data = this.readData();
	}

	readData() {
		const source = XLSX.readFile(this.filename);
		
		if (source.Sheets.length > 0) {
			console.warn(`${this.filename} contains multiple worksheets. Using only ${source.SheetNames[0]}!`);
		}
		const sheet = source.Sheets[source.SheetNames[0]];
		return XLSX.utils.sheet_to_json(sheet, {raw: false});
	}
	
	/**
	 * Matches any line of data against all the specified queries.
	 * @param {Array} query array of objects like this: {field: "a", comparison: "<", value: "def"}
	 */
	getFilteredData(query) {
		let undefined; // used later in the switch case. Do not remove!

		return this.data.filter((line) => {
			let filterResult = true;

			for (const q of query) {
				switch (q.comparison) {
				case "=":
					if (line[q.field] != q.value) { // intentionally no typesafe comparison!
						filterResult = false;
					}
					break;
				case "!":
					if (line[q.field] == q.value) { // intentionally no typesafe comparison!
						filterResult = false;
					}
					break;
				case ">":
					if (line[q.field] <= q.value) {
						filterResult = false;
					}
					break;
				case "<":
					if (line[q.field] >= q.value) {
						filterResult = false;
					}
					break;
				case undefined:
					if (!line[q.field]) {
						filterResult = false;
					}
					break;
				default:
					throw new Error(`comparison ${q.comparison} has not been implemented!`);
				}
				console.log(`  ${filterResult ? ">" : "X"} field ${q.field} "${line[q.field]}" ${q.comparison} "${q.value}" for ${Object.values(line).join(",")}`);
			}

			return filterResult;
		});
	}
}
