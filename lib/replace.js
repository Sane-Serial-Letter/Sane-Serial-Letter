function xmlReplacements(input) {
	return input
		.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;");
}

export function replace(content, data, xmlsafe = false, verbose = false) {
	return content.replace(/\{\{([a-z0-9-]+)\}\}/gi, (substring, captureGroup0,  index) => {
		let replacement = data[captureGroup0] || "";
		if (xmlsafe) {
			replacement = xmlReplacements(replacement);
		}
		if (verbose) {
			console.log(`  - replacing ${substring} with ${replacement} at index ${index}`);
		}
		return replacement;
	});
}
