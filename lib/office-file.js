import { readFile, writeFile } from "fs/promises";

import JSZip from "jszip";
export class OfficeFile {
	constructor() {
		this.zip;
		this.content;
	}

	async loadContent(templateFilename) {
		const file = await readFile(templateFilename);
		this.zip = await JSZip.loadAsync(file);
		this.content = await this.zip.file("content.xml").async("string");
	}

	async storeContentAs(content, outputFilename, foreceOverwrite = false) {
		this.zip.file("content.xml", content);
		await writeFile(outputFilename, await this.zip.generateAsync({type: "uint8array"}), {flag: foreceOverwrite ? "w" : "wx"});
	}
}
