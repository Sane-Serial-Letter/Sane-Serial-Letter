import path from "path";
import { promises as fs } from "fs";
import { promisify } from "util";

import libre from "libreoffice-convert";

const convertAsync = promisify(libre.convert);
const ext = ".pdf";

export async function pdfConversion(filename, foreceOverwrite = false) {
	const outputPath = path.join(`${filename}${ext}`);

	const inputFile = await fs.readFile(filename);
	let pdfBuf = await convertAsync(inputFile, ext, undefined);
	await fs.writeFile(outputPath, pdfBuf, {flag: foreceOverwrite ? "w" : "wx"});
}
